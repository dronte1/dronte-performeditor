use dronte_com::app::global_mode::{
    get_global_mode, set_and_save_global_workspace_properties, AppGlobalMode,
};
use dronte_com::app::logger::info;
use dronte_com::errors::{DronteError, DronteResult};

#[tauri::command]
pub async fn change_project_index(index: usize) -> DronteResult<AppGlobalMode> {
    let mode = get_global_mode();

    if let AppGlobalMode::Workspace(definition) = mode {
        let mut props = definition.get_properties();
        props.selected_project_index = index;
        info(format!(
            "Changed default project to {}",
            props.projects[index].name
        ))
        .await;
        set_and_save_global_workspace_properties(props).await
    } else {
        Err(DronteError::Mode(
            "Cannot change project index when in setup mode".to_string(),
        ))
    }
}

#[tauri::command]
pub async fn add_project(name: String) -> DronteResult<AppGlobalMode> {
    let mode = get_global_mode();

    if let AppGlobalMode::Workspace(definition) = mode {
        let mut defmut = definition.clone();
        defmut.add_project(name.clone()).await?;
        info(format!("Creating project {}", name)).await;
        set_and_save_global_workspace_properties(defmut.get_properties()).await
    } else {
        Err(DronteError::Mode(
            "Cannot add project when in setup mode".to_string(),
        ))
    }
}

#[tauri::command]
pub async fn remove_project(index: usize) -> DronteResult<AppGlobalMode> {
    let mode = get_global_mode();

    if let AppGlobalMode::Workspace(definition) = mode {
        let mut defmut = definition.clone();
        let project = defmut.remove_project(index).await?;
        info(format!("Removed project {}", project.name)).await;

        set_and_save_global_workspace_properties(defmut.get_properties()).await
    } else {
        Err(DronteError::Mode(
            "Cannot remove project when in setup mode".to_string(),
        ))
    }
}

#[tauri::command]
pub async fn clone_project(index: usize, name: String) -> DronteResult<AppGlobalMode> {
    let mode = get_global_mode();

    if let AppGlobalMode::Workspace(definition) = mode {
        let mut defmut = definition.clone();
        defmut.clone_project(index, name.clone()).await?;
        info(format!(
            "Cloned project {} from project {}",
            name,
            defmut.get_properties().projects[index].name
        ))
        .await;

        set_and_save_global_workspace_properties(defmut.get_properties()).await
    } else {
        Err(DronteError::Mode(
            "Cannot clone project when in setup mode".to_string(),
        ))
    }
}

#[tauri::command]
pub async fn rename_project(index: usize, name: String) -> DronteResult<AppGlobalMode> {
    let mode = get_global_mode();

    if let AppGlobalMode::Workspace(definition) = mode {
        let mut defmut = definition.clone();
        defmut.rename_project(index, name).await?;

        set_and_save_global_workspace_properties(defmut.get_properties()).await
    } else {
        Err(DronteError::Mode(
            "Cannot rename project when in setup mode".to_string(),
        ))
    }
}
