use dronte_com::app::global_mode::{
    get_global_mode, go_to_existing_workspace, go_to_new_workspace, AppGlobalMode,
};
use dronte_com::app::logger::{info, LogStream};
use dronte_com::errors::DronteResult;
use futures::StreamExt;
use std::path::PathBuf;
use std::time::Duration;
use tauri::{AppHandle, Manager};
use tokio::spawn;
use tokio::time::sleep;

#[tauri::command]
pub fn retrieve_global_mode() -> AppGlobalMode {
    println!("blah");
    get_global_mode()
}

#[tauri::command]
pub async fn new_workspace(path: String) -> DronteResult<()> {
    info(format!("Creating new workspace in {}", path.clone())).await;
    go_to_new_workspace(PathBuf::from(path))
}

#[tauri::command]
pub async fn open_workspace(path: String) -> DronteResult<()> {
    info(format!("Opening workspace from {}", path.clone())).await;
    go_to_existing_workspace(PathBuf::from(path))
}

pub async fn spawn_logger(app_handle: &AppHandle) -> Result<(), ()> {
    let mut log_stream = LogStream::new();
    spawn(async {
        sleep(Duration::from_secs(2)).await;
        info("Welcome to Dronte Performer Editor".to_string()).await;
        info(format!("Version {}", env!("CARGO_PKG_VERSION"))).await;
    });

    while let Some(messages) = log_stream.next().await {
        app_handle.emit_all("new-log-available", messages).unwrap();
    }

    Ok(())
}
