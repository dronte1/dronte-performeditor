// Prevents additional console window on Windows in release, DO NOT REMOVE!!
#![cfg_attr(not(debug_assertions), windows_subsystem = "windows")]

use crate::{
    app::global_app_handlers::{new_workspace, open_workspace, retrieve_global_mode, spawn_logger},
    workspace::global_workspace_handlers::{
        add_project, change_project_index, clone_project, remove_project, rename_project,
    },
};

mod app;
mod workspace;

fn main() {
    tauri::Builder::default()
        .setup(|app| {
            let handle = app.handle();
            tauri::async_runtime::spawn(async move { spawn_logger(&handle).await });
            Ok(())
        })
        .invoke_handler(tauri::generate_handler![
            retrieve_global_mode,
            open_workspace,
            new_workspace,
            change_project_index,
            add_project,
            remove_project,
            rename_project,
            clone_project,
        ])
        .run(tauri::generate_context!())
        .expect("error while running tauri application");
}
