use crate::utils::{parse_i64, parse_u32};
use serde::{Deserialize, Serialize};
use std::mem::size_of;

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct NoteValues {
    pitch: u8,
    velocity: u8,
    channel: u8,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum Note {
    On(NoteValues),
    Off(NoteValues),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct Continuous {
    number: u32,
    value: u32,
    channel: u8,
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub enum EventMessageContents {
    Discrete(Note),
    Continuous(Option<Continuous>),
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct EventMessage {
    contents: EventMessageContents,
    timestamp: i64,
}

const CONTENTS_OFFSET: usize = 10;
const CONTINUOUS_OFFSET: usize = 15;
const TIMESTAMP_OFFSET: usize = 23;

impl TryFrom<&[u8]> for EventMessage {
    type Error = Box<dyn std::error::Error>;
    fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        let continuous_number =
            parse_u32(&value[CONTINUOUS_OFFSET..CONTINUOUS_OFFSET + size_of::<u32>()])?;
        let continuous_value =
            parse_u32(&value[CONTINUOUS_OFFSET..CONTINUOUS_OFFSET + 2 * size_of::<u32>()])?;
        let contents = if value[CONTENTS_OFFSET] == 1 {
            if value[CONTENTS_OFFSET + 1] == 1 {
                EventMessageContents::Discrete(Note::On(NoteValues {
                    channel: value[CONTENTS_OFFSET + 2],
                    pitch: value[CONTENTS_OFFSET + 3],
                    velocity: value[CONTENTS_OFFSET + 4],
                }))
            } else {
                EventMessageContents::Discrete(Note::Off(NoteValues {
                    channel: value[CONTENTS_OFFSET + 2],
                    pitch: value[CONTENTS_OFFSET + 3],
                    velocity: value[CONTENTS_OFFSET + 4],
                }))
            }
        } else if continuous_number != 0 {
            EventMessageContents::Continuous(Some(Continuous {
                number: continuous_number,
                value: continuous_value,
                channel: value[CONTENTS_OFFSET + 2],
            }))
        } else {
            EventMessageContents::Continuous(None)
        };

        let timestamp = parse_i64(&value[TIMESTAMP_OFFSET..TIMESTAMP_OFFSET + size_of::<i64>()])?;

        Ok(EventMessage {
            contents,
            timestamp,
        })
    }
}
