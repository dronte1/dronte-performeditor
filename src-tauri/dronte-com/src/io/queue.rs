use futures::{Sink, Stream};
use std::collections::VecDeque;
use std::future::Future;
use std::pin::{pin, Pin};
use std::task::Poll::Pending;
use std::task::{Context, Poll};
use zeromq::{PubSocket, SocketRecv, SocketSend, SubSocket, ZmqError, ZmqMessage, ZmqResult};

pub struct SubSocketStream(SubSocket);

impl Stream for SubSocketStream {
    type Item = ZmqResult<ZmqMessage>;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        pin!(self.get_mut().0.recv()).poll(cx).map(Some)
    }
}

pub struct PubSocketSink(PubSocket, VecDeque<ZmqMessage>);

impl Sink<ZmqMessage> for PubSocketSink {
    type Error = ZmqError;
    fn poll_ready(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        if self.1.is_empty() {
            Poll::Ready(Ok(()))
        } else {
            Pending
        }
    }

    fn start_send(self: Pin<&mut Self>, item: ZmqMessage) -> Result<(), Self::Error> {
        self.get_mut().1.push_back(item);
        Ok(())
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let mutable_self = self.get_mut();
        let mut poll_vec = Vec::new();

        while let Some(item) = mutable_self.1.pop_front() {
            let pinned_poll = pin!(mutable_self.0.send(item)).poll(cx);
            poll_vec.push(pinned_poll);
        }

        if poll_vec.iter().any(|poll| poll.is_pending()) {
            Poll::Pending
        } else if poll_vec.iter().any(|poll| match poll {
            Poll::Ready(result) => result.is_err(),
            Poll::Pending => false,
        }) {
            Poll::Ready(Err(ZmqError::NoMessage))
        } else {
            Poll::Ready(Ok(()))
        }
    }

    fn poll_close(self: Pin<&mut Self>, _cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        if self.1.is_empty() {
            Poll::Ready(Ok(()))
        } else {
            Poll::Pending
        }
    }
}
