use crate::errors::{DronteError, DronteResult};
use futures::FutureExt;
use mp4::{Mp4Reader, Mp4Track};
use std::collections::HashMap;
use std::fs::File;
use std::future::Future;
use std::io::BufReader;
use std::pin::Pin;
use std::task::{Context, Poll};
use tokio::task::JoinHandle;

pub struct Mp4AsyncReader(Mp4Reader<BufReader<File>>);

impl<'a> Mp4AsyncReader {
    pub fn try_new(buf_reader: BufReader<File>, size: u64) -> Mp4AsyncReaderNew {
        Mp4AsyncReaderNew::new(buf_reader, size)
    }

    pub fn size(&self) -> u64 {
        self.0.size()
    }

    pub fn tracks(&'a self) -> &'a HashMap<u32, Mp4Track> {
        self.0.tracks()
    }

    pub(super) fn get_reader_ref(&'a self) -> &'a Mp4Reader<BufReader<File>> {
        &self.0
    }
}

pub struct Mp4AsyncReaderNew(JoinHandle<DronteResult<Mp4AsyncReader>>);

impl Mp4AsyncReaderNew {
    pub fn new(buf_reader: BufReader<File>, size: u64) -> Self {
        let join_handle = tokio::task::spawn_blocking(move || -> DronteResult<Mp4AsyncReader> {
            let reader = Mp4Reader::read_header(buf_reader, size)?;
            Ok(Mp4AsyncReader(reader))
        });

        Self(join_handle)
    }
}
impl Future for Mp4AsyncReaderNew {
    type Output = DronteResult<Mp4AsyncReader>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let self_mut = self.get_mut();
        self_mut.0
            .poll_unpin(cx)
            .map(|result| -> DronteResult<Mp4AsyncReader> {
                match result {
                    Ok(result) => result,
                    Err(e) => Err(DronteError::Runtime(e.to_string())),
                }
            })
    }
}
