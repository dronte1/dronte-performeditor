use homedir::GetHomeError;
use std::fmt::{Display, Formatter};
use std::io;
use std::io::Error;
use tauri::InvokeError;
use tokio::task::JoinError;

pub type DronteResult<T> = Result<T, DronteError>;

#[derive(Debug, Clone)]
pub enum DronteError {
    Generic,
    FsNavigate(String),
    Fs(String),
    Io(String),
    Parse(String),
    Singleton(String),
    Concurrency(String),
    Mode(String),
    Video(String),
    Runtime(String),
}

impl Display for DronteError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DronteError::Generic => write!(f, "Dronte Generic Error"),
            DronteError::FsNavigate(what) => {
                write!(f, "Error while navigating filesystem: {}", what)
            }
            DronteError::Fs(what) => write!(f, "Filesystem error: {}", what),
            DronteError::Io(what) => write!(f, "I/O error: {}", what),
            DronteError::Parse(what) => write!(f, "Parse error: {}", what),
            DronteError::Singleton(what) => write!(f, "Singleton error: {}", what),
            DronteError::Concurrency(what) => write!(f, "Concurrency error: {}", what),
            DronteError::Mode(what) => write!(f, "Mode error: {}", what),
            DronteError::Video(what) => write!(f, "Video error: {}", what),
            DronteError::Runtime(what) => write!(f, "Runtime error: {}", what),
        }
    }
}

impl std::error::Error for DronteError {}

impl From<DronteError> for InvokeError {
    fn from(value: DronteError) -> Self {
        InvokeError::from(value.to_string())
    }
}

impl From<io::Error> for DronteError {
    fn from(value: Error) -> Self {
        Self::Io(value.to_string())
    }
}

impl From<GetHomeError> for DronteError {
    fn from(value: GetHomeError) -> Self {
        Self::FsNavigate(value.to_string())
    }
}

impl From<serde_json::Error> for DronteError {
    fn from(value: serde_json::Error) -> Self {
        Self::Parse(value.to_string())
    }
}

impl From<mp4::Error> for DronteError {
    fn from(value: mp4::Error) -> Self {
        Self::Video(value.to_string())
    }
}

impl From<JoinError> for DronteError {
    fn from(value: JoinError) -> Self {
        Self::Runtime(value.to_string())
    }
}
