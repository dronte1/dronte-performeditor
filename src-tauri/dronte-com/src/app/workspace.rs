use crate::errors::{DronteError, DronteResult};
use serde::{Deserialize, Serialize};
use std::fs::{create_dir, File};
use std::io::Write;
use std::path::PathBuf;
use tokio::fs::OpenOptions;
use tokio::io::AsyncWriteExt;

const PROPERTIES_PATH: &str = "dronte.properties.json";
static mut INSTANCE_FLAG: Option<()> = None;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkspaceDefinition {
    path: PathBuf,
    props: WorkspaceProperties,
}

impl WorkspaceDefinition {
    pub fn try_new(path: PathBuf) -> DronteResult<Self> {
        unsafe {
            if INSTANCE_FLAG.is_none() {
                INSTANCE_FLAG = Some(());
            } else {
                return Err(DronteError::Singleton(
                    "Only one instance of workspace can exist at a time".to_string(),
                ));
            }
        }

        let props = WorkspaceProperties::new(path.clone());
        setup_workspace(path.clone(), props.clone())?;

        Ok(Self { path, props })
    }

    pub fn set_properties(&mut self, properties: WorkspaceProperties) {
        self.props = properties;
    }

    pub fn get_properties(&self) -> WorkspaceProperties {
        self.props.clone()
    }

    pub async fn save(&self) -> DronteResult<()> {
        save_workspace(self.path.clone(), self.props.clone()).await
    }

    pub async fn add_project(&mut self, project_name: String) -> DronteResult<()> {
        let mut new_project = WorkspaceProject::new(self.path.clone());
        new_project.name = project_name;
        self.props.projects.push(new_project);
        save_workspace(self.path.clone(), self.props.clone()).await
    }

    pub async fn remove_project(&mut self, project_index: usize) -> DronteResult<WorkspaceProject> {
        let project = self.props.projects.remove(project_index);
        save_workspace(self.path.clone(), self.props.clone()).await?;

        Ok(project)
    }

    pub async fn clone_project(
        &mut self,
        project_index: usize,
        new_name: String,
    ) -> DronteResult<()> {
        let mut cloned = self.props.projects[project_index].clone();
        cloned.name = new_name;
        self.props.projects.push(cloned);
        save_workspace(self.path.clone(), self.props.clone()).await
    }

    pub async fn rename_project(
        &mut self,
        project_index: usize,
        new_name: String,
    ) -> DronteResult<()> {
        self.props.projects[project_index].name = new_name;

        save_workspace(self.path.clone(), self.props.clone()).await
    }
}

impl Drop for WorkspaceDefinition {
    fn drop(&mut self) {
        unsafe {
            INSTANCE_FLAG = None;
        }
    }
}

impl TryFrom<PathBuf> for WorkspaceDefinition {
    type Error = DronteError;

    fn try_from(value: PathBuf) -> Result<Self, Self::Error> {
        let props_file = File::open(properties_path(value.clone()))?;
        let props: WorkspaceProperties = serde_json::from_reader(props_file)?;

        check_workspace(props.clone())?;

        Ok(WorkspaceDefinition { path: value, props })
    }
}

fn setup_workspace(path: PathBuf, props: WorkspaceProperties) -> DronteResult<()> {
    for path in props
        .clone()
        .projects
        .iter()
        .flat_map(|project| project.media_pools.iter())
    {
        create_dir(path)?;
    }
    let json_contents = serde_json::to_string(&props)?;

    let mut properties_files = File::create(properties_path(path))?;
    properties_files.write_all(json_contents.as_bytes())?;

    Ok(())
}

fn check_workspace(props: WorkspaceProperties) -> DronteResult<()> {
    for path in props
        .clone()
        .projects
        .iter()
        .flat_map(|project| project.media_pools.iter())
    {
        if !path.exists() {
            return Err(DronteError::FsNavigate(format!(
                "Expected media pool path '{}' not present in workspace",
                path.to_str().unwrap_or("Unprocessable Path")
            )));
        }
    }

    for path in props
        .clone()
        .projects
        .iter()
        .flat_map(|project| project.tracks.iter())
    {
        if !path.exists() {
            return Err(DronteError::FsNavigate(format!(
                "Expected track file '{}' not present in workspace",
                path.to_str().unwrap_or("Unprocessable path")
            )));
        }
    }

    Ok(())
}

pub(crate) async fn save_workspace(
    path: PathBuf,
    properties: WorkspaceProperties,
) -> DronteResult<()> {
    check_workspace(properties.clone())?;
    let json_contents = serde_json::to_string(&properties)?;
    let mut props_file = OpenOptions::new()
        .write(true)
        .truncate(true)
        .open(properties_path(path))
        .await?;
    props_file.write_all(json_contents.as_bytes()).await?;

    Ok(())
}

fn properties_path(path: PathBuf) -> PathBuf {
    let mut properties_path = path;
    properties_path.push(PROPERTIES_PATH);

    properties_path
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkspaceProperties {
    #[serde(rename = "dronteVersion")]
    pub dronte_version: String,
    pub projects: Vec<WorkspaceProject>,
    #[serde(rename = "selectedProjectIndex")]
    pub selected_project_index: usize,
}

impl WorkspaceProperties {
    pub fn new(root_path: PathBuf) -> Self {
        let mut pool_path = root_path.clone();
        pool_path.push("default-media-pool");

        WorkspaceProperties {
            dronte_version: env!("CARGO_PKG_VERSION").to_string(),
            projects: vec![WorkspaceProject::new(root_path)],
            selected_project_index: 0,
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct WorkspaceProject {
    pub name: String,
    #[serde(rename = "mediaPools")]
    pub media_pools: Vec<PathBuf>,
    pub tracks: Vec<PathBuf>,
}

impl WorkspaceProject {
    pub fn new(root_path: PathBuf) -> Self {
        let mut pool_path = root_path;
        pool_path.push("default-media-pool");

        WorkspaceProject {
            name: "base".to_string(),
            media_pools: vec![pool_path],
            tracks: Vec::new(),
        }
    }
}
