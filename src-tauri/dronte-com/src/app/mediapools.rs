use crate::app::logger::{error, info};
use crate::errors::{DronteError, DronteResult};
use crate::io::mp4_async_reader::Mp4AsyncReader;
use mp4::{Mp4Track, TrackType};
use serde::{Deserialize, Serialize};
use std::fs::File;
use std::io::BufReader;
use std::path::PathBuf;
use std::time::Duration;
use tokio::fs::read_dir;
use tokio::task;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MediaPool {
    name: String,
    path: PathBuf,
    items: Vec<MediaPoolItem>,
}

impl MediaPool {
    pub async fn try_new(path: PathBuf) -> DronteResult<Self> {
        let mut dir_list = read_dir(path.clone()).await?;
        let mut items = Vec::new();

        while let Some(entry) = dir_list.next_entry().await? {
            if entry.metadata().await?.is_file() {
                match MediaPoolItem::try_new(entry.path()).await {
                    Ok(media_item) => items.push(media_item),
                    Err(e) => {
                        info(format!("Ignoring item {:?}, got error {}", entry.path(), e)).await;
                    }
                }
            }
        }

        if path.exists() {
            Ok(Self {
                name: path
                    .file_name()
                    .map(|os_str| os_str.to_str().unwrap_or("unprocessable").to_string())
                    .unwrap_or_else(|| "unprocessable".to_string()),
                path: path.clone(),
                items,
            })
        } else {
            Err(DronteError::FsNavigate(format!(
                "Unable to locate media pool at {}",
                path.to_str().unwrap_or("unprocessable")
            )))
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MediaPoolItem {
    path: PathBuf,
    name: String,
    size: u64,
    tracks: Vec<MediaItemVideoTrack>,
}

impl MediaPoolItem {
    pub async fn try_new(path: PathBuf) -> DronteResult<Self> {
        let pc = path.clone();
        let (file, size) = task::spawn_blocking(move || -> DronteResult<(File, u64)> {
            let file = File::open(pc)?;
            let size = file.metadata()?.len();
            Ok((file, size))
        })
        .await??;
        let reader = BufReader::new(file);
        let mp4 = Mp4AsyncReader::try_new(reader, size).await?;
        let name = path
            .file_name()
            .map(|os_s| os_s.to_str().unwrap_or("unprocessable"))
            .map(|str| str.to_string())
            .unwrap_or_else(|| "unprocessable".to_string());
        Ok(Self {
            path,
            name,
            size: mp4.size(),
            tracks: mp4
                .tracks()
                .values()
                .filter(|track| match track.track_type() {
                    Ok(t) => matches!(t, TrackType::Video),
                    Err(e) => {
                        let e_string = e.to_string();
                        tokio::spawn(async move {
                            error(format!("Error while processing video track : {}", e_string))
                                .await;
                        });
                        false
                    }
                })
                .filter_map(|video_track| {
                    match MediaItemVideoTrack::try_from(video_track) {
                        Ok(media_video_track) => Some(media_video_track),
                        Err(e) => {
                            let ec = e.clone();
                            tokio::spawn(async move {
                                error(format!("Error while processing video track : {}", ec)).await;
                            });
                            None
                        }
                    }
                })
                .collect(),
        })
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct MediaItemVideoTrack {
    id: u32,
    media_type: String,
    width: u16,
    height: u16,
    frame_rate: f64,
    duration: Duration,
}

impl TryFrom<&Mp4Track> for MediaItemVideoTrack {
    type Error = DronteError;

    fn try_from(value: &Mp4Track) -> Result<Self, Self::Error> {
        Ok(Self {
            id: value.track_id(),
            media_type: value.media_type()?.to_string(),
            width: value.width(),
            height: value.height(),
            frame_rate: value.frame_rate(),
            duration: value.duration(),
        })
    }
}
