use crate::app::global_mode::AppGlobalMode::{Setup, Workspace};
use crate::app::workspace::{WorkspaceDefinition, WorkspaceProperties};
use crate::errors::{DronteError, DronteResult};
use serde::{Deserialize, Serialize};
use std::path::PathBuf;
use tokio::sync::Mutex;

static mut GLOBAL_MODE: AppGlobalMode = Setup;
static mut MODE_MUTEX: Option<Mutex<()>> = None;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum AppGlobalMode {
    #[serde(rename = "setup")]
    Setup,
    #[serde(rename = "workspace")]
    Workspace(WorkspaceDefinition),
}

impl AppGlobalMode {
    pub fn setup() -> Self {
        Setup
    }

    pub fn workspace(path: PathBuf, create: bool) -> Result<Self, DronteError> {
        if path.is_dir() {
            if create {
                Ok(Workspace(WorkspaceDefinition::try_new(path)?))
            } else {
                Ok(Workspace(WorkspaceDefinition::try_from(path)?))
            }
        } else {
            Err(DronteError::FsNavigate(format!(
                "Path {} is not a directory",
                path.to_str().unwrap_or("unprocessable")
            )))
        }
    }
}

unsafe fn check_mutex() {
    if MODE_MUTEX.is_none() {
        MODE_MUTEX = Some(Mutex::new(()));
    }
}

pub fn go_to_setup() {
    unsafe {
        check_mutex();
        GLOBAL_MODE = AppGlobalMode::setup();
    }
}

pub fn go_to_existing_workspace(path: PathBuf) -> DronteResult<()> {
    unsafe {
        check_mutex();
        GLOBAL_MODE = AppGlobalMode::workspace(path, false)?;
    }
    Ok(())
}

pub fn go_to_new_workspace(path: PathBuf) -> DronteResult<()> {
    unsafe {
        check_mutex();
        GLOBAL_MODE = AppGlobalMode::workspace(path, true)?;
    }

    Ok(())
}

pub fn get_global_mode() -> AppGlobalMode {
    unsafe { GLOBAL_MODE.clone() }
}

pub async fn set_and_save_global_workspace_properties(
    props: WorkspaceProperties,
) -> DronteResult<AppGlobalMode> {
    let mutex_option = unsafe { &mut MODE_MUTEX };

    if let Some(ref mut mutex) = mutex_option {
        let guard = mutex.lock().await;
        let mode = unsafe { &mut GLOBAL_MODE };

        if let AppGlobalMode::Workspace(ref mut definition) = mode {
            definition.set_properties(props);
            definition.save().await?;
        }
        let new_mode = mode.clone();
        drop(guard);

        Ok(new_mode)
    } else {
        Err(DronteError::Concurrency(
            "Unable to acquire global mode mutex as it is not defined".to_string(),
        ))
    }
}
