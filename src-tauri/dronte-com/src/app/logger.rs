use chrono::Local;
use futures::{Sink, SinkExt, Stream};
use ringbuffer::{AllocRingBuffer, RingBuffer};
use serde::{Deserialize, Serialize};
use std::collections::VecDeque;
use std::future::Future;
use std::pin::{pin, Pin};
use std::sync::atomic::{AtomicUsize, Ordering};
use std::sync::Arc;
use std::task::{Context, Poll, Waker};
use std::time::Duration;
use tokio::spawn;
use tokio::time::sleep;
use tokio::{sync::RwLock, task::JoinHandle};

static mut GLOBAL_LOG: Option<RwLock<AllocRingBuffer<LogInfoWithTime>>> = None;
static mut LOG_COUNTER: Option<Arc<AtomicUsize>> = None;

const LOG_LATENCY_MS: u64 = 10;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub enum LogInfo {
    #[serde(rename = "debug")]
    Debug(String),
    #[serde(rename = "info")]
    Info(String),
    #[serde(rename = "warn")]
    Warn(String),
    #[serde(rename = "error")]
    Error(String),
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LogInfoWithTime {
    pub info: LogInfo,
    pub time: String,
}

impl From<LogInfo> for LogInfoWithTime {
    fn from(value: LogInfo) -> Self {
        LogInfoWithTime {
            info: value,
            time: Local::now().to_rfc3339(),
        }
    }
}

fn wait_and_wake<T>(waker: Waker) -> Poll<T> {
    spawn(async move {
        sleep(Duration::from_millis(LOG_LATENCY_MS)).await;
        waker.wake();
    });

    Poll::Pending
}

#[derive(Debug, Clone)]
struct Logger(VecDeque<LogInfoWithTime>);

impl Logger {
    pub fn new() -> Self {
        unsafe {
            if GLOBAL_LOG.is_none() {
                GLOBAL_LOG = Some(RwLock::new(AllocRingBuffer::new(100)));
            }

            if LOG_COUNTER.is_none() {
                LOG_COUNTER = Some(Arc::new(AtomicUsize::new(0)));
            }
        }

        Logger(VecDeque::new())
    }
}

impl Sink<LogInfo> for Logger {
    type Error = Box<dyn std::error::Error>;

    fn poll_ready(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        if let Some(rwlock) = unsafe { GLOBAL_LOG.as_ref() } {
            pin!(rwlock.read()).poll(cx).map(|_| Ok(()))
        } else {
            Poll::Pending
        }
    }

    fn start_send(self: Pin<&mut Self>, item: LogInfo) -> Result<(), Self::Error> {
        self.get_mut().0.push_back(LogInfoWithTime::from(item));

        Ok(())
    }

    fn poll_flush(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        let mutable_self = self.get_mut();
        let rwlock = unsafe { GLOBAL_LOG.as_ref() }.unwrap();
        let item = mutable_self.0.pop_front();
        let waker = cx.waker().clone();

        if let Poll::Ready(mut guard) = pin!(rwlock.write()).poll(cx) {
            if let Some(log) = item {
                guard.push(log);
                unsafe {
                    LOG_COUNTER
                        .as_ref()
                        .cloned()
                        .unwrap()
                        .fetch_add(1, Ordering::SeqCst)
                };

                wait_and_wake(waker)
            } else {
                Poll::Ready(Ok(()))
            }
        } else {
            wait_and_wake(waker)
        }
    }

    fn poll_close(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
        if self.0.is_empty() {
            Poll::Ready(Ok(()))
        } else {
            let waker = cx.waker().clone();
            wait_and_wake(waker)
        }
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LogMessages {
    pub messages: Vec<LogInfoWithTime>,
}

#[derive(Debug, Clone)]
pub struct LogStream(Logger, usize);

impl LogStream {
    pub fn new() -> Self {
        LogStream(Logger::new(), 0)
    }
}

impl Default for LogStream {
    fn default() -> Self {
        Self::new()
    }
}

impl Stream for LogStream {
    type Item = LogMessages;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<Self::Item>> {
        let mutable_self = self.get_mut();

        if let Some(rwlock) = unsafe { &GLOBAL_LOG } {
            let waker = cx.waker().clone();
            if let Poll::Ready(guard) = pin!(rwlock.read()).poll(cx) {
                let counter = unsafe {
                    LOG_COUNTER
                        .as_ref()
                        .cloned()
                        .unwrap()
                        .load(Ordering::SeqCst)
                };
                if counter != mutable_self.1 {
                    mutable_self.1 = counter;
                    Poll::Ready(Some(LogMessages {
                        messages: guard.to_vec(),
                    }))
                } else {
                    wait_and_wake(waker)
                }
            } else {
                wait_and_wake(waker)
            }
        } else {
            Poll::Ready(None)
        }
    }
}

pub async fn debug(message: String) -> JoinHandle<()> {
    spawn(async move {
        let mut logger = Logger::new();

        logger
            .send(LogInfo::Debug(message))
            .await
            .expect("Unable to log");
    })
}

pub async fn info(message: String) -> JoinHandle<()> {
    spawn(async move {
        let mut logger = Logger::new();
        logger
            .send(LogInfo::Info(message))
            .await
            .expect("Unable to log");
    })
}

pub async fn warn(message: String) -> JoinHandle<()> {
    spawn(async move {
        let mut logger = Logger::new();

        logger
            .send(LogInfo::Warn(message))
            .await
            .expect("Unable to log");
    })
}

pub async fn error(message: String) -> JoinHandle<()> {
    spawn(async move {
        let mut logger = Logger::new();

        logger
            .send(LogInfo::Error(message))
            .await
            .expect("Unable to log");
    })
}
