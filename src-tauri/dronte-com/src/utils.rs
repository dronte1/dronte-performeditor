use std::mem::size_of;

pub fn parse_u32(slice: &[u8]) -> Result<u32, Box<dyn std::error::Error>> {
    if slice.len() < size_of::<u32>() {
        Err(format!(
            "Buffer of size {} cannot be parsed into a 32 bit unsigned int",
            slice.len()
        )
        .into())
    } else {
        let mut buffer = [0u8; size_of::<u32>()];
        for (i, v) in slice.iter().enumerate() {
            buffer[i] = *v;
        }

        Ok(u32::from_le_bytes(buffer))
    }
}

pub fn parse_i64(slice: &[u8]) -> Result<i64, Box<dyn std::error::Error>> {
    if slice.len() < size_of::<i64>() {
        Err(format!(
            "Buffer of size {} cannot be parsed into a 32 bit unsigned int",
            slice.len()
        )
        .into())
    } else {
        let mut buffer = [0u8; size_of::<i64>()];
        for (i, v) in slice.iter().enumerate() {
            buffer[i] = *v;
        }

        Ok(i64::from_le_bytes(buffer))
    }
}
