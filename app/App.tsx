'use client';

import { createTheme } from '@mui/material/styles';
import {ThemeProvider} from "@emotion/react";
import GlobalMode from "@/app/GlobalMode";
import Console from "@/app/common/Console";

const darkTheme = createTheme({
    palette: {
        mode: 'dark',
    }
});


export default function DronteApp() {
    return <ThemeProvider theme={darkTheme}>
        <GlobalMode />
        <Console />
    </ThemeProvider>
}