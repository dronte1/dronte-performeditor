'use client';

import {MutableRefObject, useEffect, useRef, useState} from "react";
import {LogMessage} from "@/app/lib/types";
import {logMessages$} from "@/app/lib/flows";

export default function Console() {
    const [messages, setMessages] = useState<Array<LogMessage>>(() => new Array<LogMessage>());
    const scrollRef: MutableRefObject<HTMLParagraphElement | null> = useRef(null);

    useEffect(() => {
        logMessages$
            .subscribe((payload) => {
                setMessages(payload.messages);
                scrollRef.current?.scrollIntoView();
            })
    });

    const lines = messages.map((logInfo, index) => {
       const key = Object.keys(logInfo.info).find((k) => ['debug', 'info', 'warn', 'error'].some((i) => i === k))!;
       const className = `${key}_log_messages log_message_flex`;
       return index < messages.length - 1 ?
           <p className={className}>
                <div className="console_line_time">[{logInfo.time}]</div>
                <div className="console_line_message">{logInfo.info[key as keyof typeof logInfo.info]}</div>
            </p>
           :
           <p className={className} ref={scrollRef}>
               <div className="console_line_time">[{logInfo.time}]</div>
               <div className="console_line_message">{logInfo.info[key as keyof typeof logInfo.info]}</div>
           </p>
    });

    return <div className="console_container">
        <div className="console_controls">ctls</div>
        <div className="console_main">
        <h3 className="console_label">Console</h3>
            <div className="console_lines">
                {lines}
            </div>
        </div>
    </div>
}
