import {AlertProps} from "@/app/lib/props-interfaces";
import {Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@mui/material";
import Button from "@mui/material/Button";

export default function Alert(props: AlertProps) {
    const { open, onClose, message } = props;

    return <Dialog open={open} onClose={onClose}>
        <DialogTitle>Confirm</DialogTitle>
        <DialogContent>
            <DialogContentText>{message}</DialogContentText>
        </DialogContent>
        <DialogActions sx={{ display: 'flex' }}>
            <Button sx={{ flex: 1 }} variant="outlined" onClick={() => onClose(true)}>Confirm</Button>
            <Button sx={{ flex: 1 }} variant="outlined" onClick={() => onClose(false)}>Cancel</Button>
        </DialogActions>
    </Dialog>
}