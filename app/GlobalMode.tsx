'use client';

import { useEffect, useState } from "react";
import { invoke } from "@tauri-apps/api/tauri";
import {AppGlobalMode, WorkspaceClick, WorkspaceGlobalMode} from "@/app/lib/types";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Button from '@mui/material/Button';
import {open} from "@tauri-apps/api/dialog";
import Workspace from "@/app/workspace/Workspace";


export default function GlobalMode() {
    const [globalMode, setGlobalMode] = useState<AppGlobalMode>('');
    const [defaultPath, setDefaultPath] = useState('');

    useEffect(() => {
        invoke<AppGlobalMode>('retrieve_global_mode', {})
            .then(result => setGlobalMode(result))
            .catch(console.error);
        import('@tauri-apps/api/path').then(({ homeDir }) => {
            homeDir()
                .then((dir) => {
                    setDefaultPath(dir);
                })
                .catch(console.error);
        });
    }, []);

    async function clickWorkspace(action: WorkspaceClick) {
        const selected = await open({
            multiple: false,
            directory: true,
            defaultPath: defaultPath,
        });

        if (selected) {
            await invoke(`${action}_workspace`, { path: selected });
            const newMode = await invoke<AppGlobalMode>('retrieve_global_mode', {});

            setGlobalMode(newMode);
        }
    }

    if (globalMode === 'setup') {
        return <Box sx={{ minWidth: 300}}>
            <Card variant='outlined'>
                <CardContent>
                    <Box textAlign='center'>
                        <Box sx={{ m: '10%' }}>
                            <Button variant='outlined' onClick={() => clickWorkspace('new')}>New Workspace</Button>
                        </Box>
                        <Box sx={{ m: '10%' }}>
                            <Button variant='outlined' onClick={() => clickWorkspace('open')}>Open Workspace</Button>
                        </Box>
                    </Box>
                </CardContent>
            </Card>
        </Box>
    } else if (globalMode) {
        const workspaceMode = (globalMode as WorkspaceGlobalMode);
        return <Workspace properties={workspaceMode.workspace.props} path={workspaceMode.workspace.path} />;
    } else {
        return <div>WTF ???</div>
    }
}
