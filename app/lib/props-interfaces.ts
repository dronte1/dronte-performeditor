import {ProjectAction, WorkspaceProperties} from "@/app/lib/types";

export interface WorkspaceProps {
    properties: WorkspaceProperties,
    path: string,
}

export interface CreateProjectFormProps {
    open: boolean,
    onClose: (value: string) => void;
    action: ProjectAction;
    startupValue?: string;
    cancel: () => void;
}

export interface AlertProps {
    open: boolean;
    onClose: (value: boolean) => void;
    message: string;
}

export interface PoolListProps {
    pools: [string]
}
