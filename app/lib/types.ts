export interface WorkspaceGlobalMode {
    workspace: WorkspaceDefinition;
}

export type AppGlobalMode = string | WorkspaceGlobalMode;

export type LogInfoKey = 'debug' | 'info' | 'warn' | 'error';

export type LogInfoInner = {
    [T in LogInfoKey]: string;
}

export interface LogMessage {
    info: LogInfoInner;
    time: string;
}

export interface LogMessages {
    messages: [LogMessage];
}

export interface WorkspaceDefinition {
    path: string,
    props: WorkspaceProperties,
}

export interface WorkspaceProperties {
    dronteVersion: string,
    projects: [WorkspaceProject],
    selectedProjectIndex: number,
}

export interface WorkspaceProject {
    name: string,
    mediaPools: [string],
    tracks: [string],
}

export type WorkspaceClick = 'open' | 'new';

export type ProjectAction = 'add' | 'clone' | 'remove' | 'rename';
