import {Observable} from 'rxjs';
import {listen, UnlistenFn} from "@tauri-apps/api/event";
import {LogMessages} from "@/app/lib/types";

export let stopListenLogMessages: UnlistenFn;

export const logMessages$: Observable<LogMessages> = new Observable((subscriber) => {
    listen<LogMessages>('new-log-available', (event) => {
        subscriber.next(event.payload);
    }).then((unListen) => {
        stopListenLogMessages = unListen;
    });
});
