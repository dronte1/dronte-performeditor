import DronteApp from "@/app/App";

export default function Home() {
  return (
    <main className="flex min-h-screen flex-col items-center justify-between p-24">
        <DronteApp />
    </main>
  );
}
