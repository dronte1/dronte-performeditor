'use client';
import {
    Dialog,
    DialogTitle,
    Box,
    DialogContent,
    DialogActions,
    TextField, Divider
} from "@mui/material";
import {CreateProjectFormProps} from "@/app/lib/props-interfaces";
import Button from "@mui/material/Button";
import React from "react";

export default function ProjectForm(props: CreateProjectFormProps) {
    const { open, onClose, action } = props;
    let title = "default";

    switch (action) {
        case "add":
            title = "Create new project";
            break;
        case "rename":
            title = "Rename project";
            break;
        case "clone":
            title = "Clone project";
            break;
        case "remove":
            title = "If it shows, this is a programmer error"
            break;
    }

    return <Dialog
        open={open}
        onClose={onClose}
        PaperProps={{
            component: 'form',
            onSubmit: (event: React.FormEvent<HTMLFormElement>) => {
                event.preventDefault();
                const formData = new FormData(event.currentTarget);
                const name = formData.get("projectName");

                if (name) {
                    const nameStr = name as string;
                    onClose(nameStr);
                }
            }
        }}
    >
        <DialogTitle>{title}</DialogTitle>
        <Divider />
        <DialogContent>
            <TextField
                autoFocus
                required
                variant="outlined"
                label="Project Name"
                id="projectName"
                name="projectName"
                defaultValue={props.startupValue}
                fullWidth
            />
        </DialogContent>
        <Divider />
        <DialogActions sx={{ display: 'flex' }}>
            <Button sx={{ flex: 1 }} variant="outlined" type="submit">Ok</Button>
            <Box sx={{ flex: 1 }} />
            <Button sx={{ flex: 1 }} variant="outlined" onClick={props.cancel}>Cancel</Button>
        </DialogActions>
    </Dialog>
}