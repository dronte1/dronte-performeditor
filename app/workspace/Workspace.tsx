import {WorkspaceProps} from "@/app/lib/props-interfaces";
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import {SyntheticEvent, useState} from "react";
import Media from "@/app/workspace/spaces/Media";
import {Divider, FormControl, IconButton, InputLabel, MenuItem, Select, SelectChangeEvent} from "@mui/material";
import {ContentCopy, Add, Delete, Edit} from "@mui/icons-material";
import ProjectForm from "@/app/workspace/modal/ProjectForm";
import {invoke} from "@tauri-apps/api/tauri";
import {AppGlobalMode, ProjectAction, WorkspaceGlobalMode} from "@/app/lib/types";
import Alert from "@/app/common/Alert";

export default function Workspace(props: WorkspaceProps) {
    const [selectedTab, setSelectedTab] = useState('1');
    const [selectedProject, setSelectedProject] = useState(props.properties.selectedProjectIndex);
    const [projects, setProjects] = useState(props.properties.projects);
    const [createProjectOpen, setCreateProjectOpen] = useState(false);
    const [dialogAction, setDialogAction] = useState<ProjectAction>('add');
    const [removeAlertOpen, setRemoveAlertOpen] = useState(false);
    
    function handleSelectedTabChange (event: SyntheticEvent, newValue: string) {
        setSelectedTab(newValue);
    }

    function handleSelectedProjectChange (event: SelectChangeEvent){
        invoke<AppGlobalMode>('change_project_index', { index: parseInt(event.target.value as string) })
            .then((mode) => {
                setSelectedProject((mode as WorkspaceGlobalMode).workspace.props.selectedProjectIndex);
            })
            .catch(console.error);
    }

    function handleCreateProjectClick () {
        setDialogAction('add');
        setCreateProjectOpen(true);
    }

    function handleCloneProjectClick () {
        setDialogAction('clone');
        setCreateProjectOpen(true);
    }

    function handleRenameProjectClick () {
        setDialogAction('rename');
        setCreateProjectOpen(true);
    }

    function handleRemoveProjectClick () {
        setDialogAction('remove');
        setRemoveAlertOpen(true);
    }

    function generateActionArgs(value: string) {
        switch (dialogAction) {
            case 'add':
                return {
                    name: value,
                };
            case 'remove':
                return {
                    index: selectedProject,
                };
            case 'rename':
                return {
                    index: selectedProject,
                    name: value,
                };
            case 'clone':
                return {
                    index: selectedProject,
                    name: value,
                };
        }
    }

    function generateStartupValue() {
        if (dialogAction === 'add') {
            return undefined;
        } else {
            return projects[selectedProject].name;
        }
    }

    function handleDialogClose (value: string) {
        invoke<AppGlobalMode>(`${dialogAction}_project`, generateActionArgs(value))
            .then((mode) => {
                const workspaceMode = mode as WorkspaceGlobalMode;
                const prevLength = projects.length;
                setProjects(workspaceMode.workspace.props.projects);

                if (dialogAction === 'clone') {
                    setSelectedProject(prevLength);
                    invoke<AppGlobalMode>('change_project_index', { index: prevLength })
                        .then((mode) => {
                            setSelectedProject((mode as WorkspaceGlobalMode).workspace.props.selectedProjectIndex);
                        })
                        .catch(console.error);
                }
            })
            .catch(console.error);
        setCreateProjectOpen(false);
    }

    function handleRemoveAlertClose (value: boolean) {
        if (value) {
            invoke<AppGlobalMode>('remove_project', generateActionArgs(''))
                .then((mode) => {
                    const workspaceMode = mode as WorkspaceGlobalMode;
                    invoke<AppGlobalMode>('change_project_index', { index: 0 })
                        .then((mode) => {
                            setSelectedProject((mode as WorkspaceGlobalMode).workspace.props.selectedProjectIndex);
                            setProjects(workspaceMode.workspace.props.projects);
                        })
                        .catch(console.error);
                });
        }
        setRemoveAlertOpen(false);
    }

    return <Box sx={{ width: '110%', mt: '-3%', typography: 'body1'}}>
        <Box sx={{ display: 'flex' }}>
            <FormControl sx={{ flex: 6 }} fullWidth>
                <InputLabel id="project-selector-label">Project</InputLabel>
                <Select
                    labelId="project-selector-label-id"
                    id="project-selector"
                    value={`${selectedProject}`}
                    label="Project"
                    onChange={handleSelectedProjectChange}
                >
                    {projects.map((project, index) => {
                        return <MenuItem key={`${index}`} value={`${index}`}>{project.name}</MenuItem>
                    })}
                </Select>
            </FormControl>
            <Box sx={{ flex: 1}}></Box>
            <IconButton sx={{ flex: 1 }} aria-label="create-project" onClick={handleCreateProjectClick}>
                <Add />
            </IconButton>
            <IconButton disabled={projects[selectedProject].name === 'base'} sx={{ flex: 1 }} aria-label="delete-project" onClick={handleRemoveProjectClick}>
                <Delete />
            </IconButton>
            <IconButton sx={{ flex: 1 }} aria-label="clone-project" onClick={handleCloneProjectClick}>
                <ContentCopy />
            </IconButton>
            <IconButton disabled={projects[selectedProject].name === 'base'} sx={{ flex: 1 }} aria-label="edit-project-name" onClick={handleRenameProjectClick}>
                <Edit />
            </IconButton>
            <Box sx={{ flex: 40 }}></Box>
        </Box>
        <Divider />
        <TabContext value={selectedTab}>
            <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                <TabList onChange={handleSelectedTabChange} aria-label="Space tabs">
                    <Tab label="Media" value="1" />
                    <Tab label="Edit" value="2" />
                    <Tab label="Nodes" value="3" />
                </TabList>
            </Box>
            <TabPanel value="1"><Media /></TabPanel>
            <TabPanel value="2">In Edit</TabPanel>
            <TabPanel value="3">In Nodes</TabPanel>
        </TabContext>
        <ProjectForm open={createProjectOpen} onClose={handleDialogClose} cancel={() => setCreateProjectOpen(false)} action={dialogAction} startupValue={generateStartupValue()}/>
        <Alert open={removeAlertOpen} onClose={handleRemoveAlertClose} message={`Are you sure you do want to delete project ${projects[selectedProject].name} ?`} />
    </Box>;
}